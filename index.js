$(document).ready(function(){
    var human = new Human('test');

    $('#human-get-state').click(function() {
        console.log(human.getState());
    });

    $('#human-set-state').click(function() {
        human.setState({
            head: true,
        })
    });
});
